# KlassDB

KlassDB is an object-oriented database which aims to provide a solution to store data in an efficient way.



## Overview

The database stores arrays of objects. An array works with only one class, which is defined at array creation.

Classes are declared using **CDL** (Class Declaration Language) and are stored in the database in binary format.

Arrays content can be accessed or modified by performing requests to the database.



## Files structure

The files structure in the database is the following:

```
arrays/					# The directory containing all arrays
	foo.bin				# The `foo` array
classes/				# The directory containing all classes
	foo.cdl				# The `foo` class
processes/				# The directory containing all processes
	foo._construct.so	# The process for the constructor of the `foo` class
users/					# The directory containing all users
	root.json			# The root user's file
config.json				# The database configuration file
data.bin				# The file containing all the data of the database in binary format
database.json			# The database informations file
logs.log				# The logs file
```



## Arrays

Arrays are the basic components of the database storage.

Each array has several information:
- **Name**: Used to identify the array
- **Description**: A description about the array
- **Class**: The class the array is going to work with

TODO



## CDL

CDL (Class Declaration Language) is a language allowing to define classes that will be used by the database.

A class can contain:
- **Variable**: Place where data can be stored
- **Function**: Link to a process that can be performed on this class
- **Constructor**: A function called on object creation
- **Destructor**: A function called on object deletion

Classes are declared like so:

```
class name
	content
```

Here, ``name`` is the class's name and ``content`` is the class's content.



### Variables

A variables is an entity that has 3 properties: a type (which defines its size), a name (which allow to identify it) and a value (which is the data stored into it).

A variable must be declared inside a class and thus, is used to store data into objects.

Variables are declared like so:

```
type name
```

or:

```
type name = value
```

``type`` specify the type of the variable, which can be one of the following:

| #  | Name   | Value type         | Size (bytes) | Signed |
|----|--------|--------------------|--------------|--------|
| 0  | int8   | integer            | 1            | yes    |
| 1  | uint8  | integer            | 1            | no     |
| 2  | int16  | integer            | 2            | yes    |
| 3  | uint16 | integer            | 2            | no     |
| 4  | int32  | integer            | 4            | yes    |
| 5  | uint32 | integer            | 4            | no     |
| 6  | int64  | integer            | 8            | yes    |
| 7  | uint64 | integer            | 8            | no     |
| 8  | float  | floating-point     | 4            | yes    |
| 9  | double | floating-point     | 8            | yes    |
| 10 | string | character sequence | 4\*          | no     |
| 11 | \*\*   | object             | 4\*          | no     |

\*: Actually an address to the data
\*\*: Must be the name of the object's class

``name`` is the name of the variable, allowing to identify it. Two variables in the same class can't have the same name.

``value`` is the value contained into the variable. It must fit into the variable according to its type.
Values can take several forms:
- **Character**: Declared like this: ``'a'``, represents a charcter and is the equivalent to a number of value of the ASCII value of the given charcter.
- **Integer**: Any non-floating point number (decmial, hexadecimal or power notation).
- **Floating-point number**: Any number (decimal or power notation).
- **Mathematical expression**: Actually, resolved at compile-type and considered just like an integer or a floating-point number according to its content.
- **Boolean**: ``true`` or ``false``, just like ``1`` or ``0`` respectively.
- **String**: A character sequence delimited by ``"``

**Note**: For character or string declaration, the delimiter can be cancelled using ``\``.



#### Mathematical expressions

Mathematical expressions can take any number as operand and **+**, **-**, **\***, **/** and **%** as operator.



### Functions

A function is a link between the class and a process of the same name. If a function is invoked for an object, the database will perform the process linked to it for this object.

For a given function, the database will look for a process called **class.function** where **class** is the class's name and **function** is the function's name.

Functions are declared like so:

```
return name()
```

or:

```
return name(args)
```

Here, ``return`` is the return type of the function, namely a value that will be returned after the function has terminated its execution. This word can be either:
- A variable type
- ``void`` (In this case, the function returns nothing)

``name`` is the name of the function. Two functions in the same class can't have the same name, unless they have different arguments.

``args`` is the arguments list of the function. If it exists, it means that the function will need parameters to be invoked.
The arguments can be declared one after the other, separated by commas.



#### Constructors

Constructors are functions that are invoked when the object is created. Otherwise, they work just like usual functions.

Constructors are declared like so:

```
_construct()
```

or:

```
_construct(args)
```



#### Destructors

Destructors are functions that are invoked when the object is deleted. Otherwise, they work just like usual functions.

Destructors are declared like so:

```
_destruct()
```



## Requests

Clients of the database can send requests to access or modifiy data on the database.

TODO



## Processes

TODO
