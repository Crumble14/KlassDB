#ifndef KLASS_DB_HPP
# define KLASS_DB_HPP

# include<fstream>
# include<iostream>
# include<memory>
# include<sstream>

# include "command/command.hpp"
# include "manager/manager.hpp"
# include "util/util.hpp"

# define VERSION				"0.1"

# define CONFIG_FILE			"config.json"
# define DATABASE_INFOS_FILE	"database.json"
# define LOGS_FILE				"logs.log"

namespace KlassDB
{
	using namespace std;

	class Database
	{
		public:
			Database();

			inline unsigned long get_start_timestamp() const
			{
				return start;
			}

			string get_uptime() const;

			void log(const string&& log);

			void init();

			inline DataManager& get_data_manager()
			{
				return data_manager;
			}

			inline ProcessesManager& get_processes_manager()
			{
				return processes_manager;
			}

			inline UsersManager& get_users_manager()
			{
				return users_manager;
			}

			inline ClientsManager& get_clients_manager()
			{
				return clients_manager;
			}

			template<typename C>
			inline void register_command()
			{
				commands.emplace_back(new C());
			}

			inline const vector<unique_ptr<Command>>& get_commands() const
			{
				return commands;
			}

			const Command* get_command(const string& name) const;
			bool exec_command(const string& command);

		private:
			unsigned long start = get_timestamp();

			ofstream logs;

			DataManager data_manager;
			ProcessesManager processes_manager;
			UsersManager users_manager;
			ClientsManager clients_manager;

			vector<unique_ptr<Command>> commands;

			void init_logs();
	};
}

#endif
