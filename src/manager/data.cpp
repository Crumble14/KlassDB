#include "manager.hpp"
#include "../klass_db.hpp"

using namespace KlassDB;

void DataManager::init()
{
	db->log("Initializing data manager...");

	mkdir(ARRAYS_DIR, S_IRUSR | S_IWUSR);
	mkdir(CLASSES_DIR, S_IRUSR | S_IWUSR);

	// TODO
}
