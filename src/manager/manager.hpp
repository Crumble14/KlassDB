#ifndef MANAGER_HPP
# define MANAGER_HPP

# include<string>
# include<vector>

# include<sys/stat.h>
# include<sys/types.h>

# include "../parser/class/class_parser.hpp"

# define ARRAYS_DIR		"arrays/"
# define CLASSES_DIR	"classes/"
# define PROCESSES_DIR	"processes/"
# define USERS_DIR		"users/"

# define DATA_FILE		"data.bin"

namespace KlassDB
{
	using namespace std;

	class Database;

	class Manager
	{
		public:
			Manager() = default;

			inline Manager(Database& database)
				: db{&database}
			{}

			virtual void init() = 0;

			inline Database& getDatabase()
			{
				return *db;
			}

		protected:
			Database* db;
	};

	class Array
	{
		public:
			inline Array(const string& name, const Class& array_class)
				: name{name}, array_class{array_class}
			{}

			inline const string& get_name() const
			{
				return name;
			}

			inline const string& get_description() const
			{
				return description;
			}

			inline void set_description(const string& description)
			{
				this->description = description;
			}

			inline const Class& get_class() const
			{
				return array_class;
			}

		private:
			const string name;
			string description;
			const Class array_class;
	};

	class DataManager : Manager
	{
		public:
			DataManager() = default;

			inline DataManager(Database& database)
				: Manager(database)
			{}

			void init() override;

		private:
			vector<Array> arrays;
	};

	class Process
	{
		public:
			// TODO

		private:
			// TODO
	};

	class ProcessesManager : Manager
	{
		public:
			ProcessesManager() = default;

			inline ProcessesManager(Database& database)
				: Manager(database)
			{}

			void init() override;

		private:
			vector<Process> processes;
	};

	class User
	{
		public:
			// TODO

		private:
			// TODO
	};

	class UsersManager : Manager
	{
		public:
			UsersManager() = default;

			inline UsersManager(Database& database)
				: Manager(database)
			{}

			void init() override;

		private:
			vector<User> users;
	};

	class Client
	{
		public:
			// TODO

		private:
			// TODO
	};

	class ClientsManager : Manager
	{
		public:
			ClientsManager() = default;

			inline ClientsManager(Database& database)
				: Manager(database)
			{}

			void init() override;

		private:
			vector<Client> clients;
	};
}

#endif
