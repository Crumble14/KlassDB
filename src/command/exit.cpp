#include "command.hpp"
#include "../klass_db.hpp"

using namespace KlassDB;

void ExitCommand::perform(Database&, const vector<string>&) const
{
	exit(0);
}
