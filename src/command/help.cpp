#include "command.hpp"
#include "../klass_db.hpp"

using namespace KlassDB;

void HelpCommand::perform(Database& db, const vector<string>&) const
{
	cout << "KlassDB version " << VERSION << '\n';
	cout << '\n';

	for(const auto& c : db.get_commands()) {
		cout << c->get_usage() << "\t\t" << c->get_description() << '\n';
	}
}
