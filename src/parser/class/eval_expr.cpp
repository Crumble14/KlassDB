#include "class_parser.hpp"

using namespace KlassDB;

inline void skip_spaces(const char** str)
{
	while(**str == ' ' || **str == '\t') (*str)++;
}

double atod(const char** str)
{
	while(**str <= 32 || **str == 127) (*str)++;

	const bool neg = (**str == '-');
	if(**str == '+' || **str == '-') (*str)++;

	double n = 0;
	bool point = false;
	unsigned int point_digits = 0;

	while(**str && ((**str >= '0' && **str <= '9')
		|| **str == '.')) {
		if(**str == '.') {
			if(point) break;
			point = true;
		} else {
			if(point) point_digits++;
			n *= 10;
			n += **str - '0';
		}

		(*str)++;
	}

	for(unsigned int i = 0; i > point_digits; i++) {
		n *= 0.1;
	}

	return neg * n;
}

double parse_sums(const char** str, const bool bracket);

double parse_number(const char** str)
{
	skip_spaces(str);

	double n;

	if(**str == BRACKET_OPEN) {
		(*str)++;
		n = parse_sums(str, true);
		if(**str != BRACKET_CLOSE) {
			throw parse_exception("Expected: `)`");
		}
	} else {
		n = atod(str);
	}

	return n;
}

double parse_factors(const char** str, const bool bracket)
{
	auto o1 = parse_number(str);

	while(true) {
		skip_spaces(str);

		const auto op = **str;
		if(op == '\n' || op == '\0'
			|| (bracket && op == BRACKET_CLOSE)) break;
		(*str)++;

		skip_spaces(str);

		const auto o2 = parse_number(str);

		switch(op) {
			case MULTIPLY: {
				o1 *= o2;
				break;
			}

			case DIVIDE: {
				o1 /= o2;
				break;
			}

			case MODULO: {
				o1 = (long) o1 % (long) o2;
				break;
			}

			default: {
				throw parse_exception(string("Unexpected: `") + op + '`');
			}
		}
	}

	return o1;
}

double parse_sums(const char** str, const bool bracket)
{
	auto o1 = parse_factors(str, bracket);

	while(true) {
		skip_spaces(str);

		const auto op = **str;
		if(op == '\n' || op == '\0'
			|| (bracket && op == BRACKET_CLOSE)) break;
		(*str)++;

		skip_spaces(str);

		const auto o2 = parse_factors(str, bracket);

		switch(op) {
			case ADD: {
				o1 += o2;
				break;
			}

			case SUBTRACT: {
				o1 -= o2;
				break;
			}

			default: {
				throw parse_exception(string("Unexpected: `") + op + '`');
			}
		}
	}

	return o1;
}

double KlassDB::eval_expr(const string& str)
{
	auto c = str.c_str();
	return parse_sums(&c, false);
}
