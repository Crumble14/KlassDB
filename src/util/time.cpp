#include "util.hpp"

using namespace KlassDB;

unsigned long KlassDB::get_timestamp()
{
	using namespace std::chrono;
	return duration_cast<milliseconds>(system_clock::now()
		.time_since_epoch()).count();
}

string KlassDB::get_time()
{
	const auto t = time(0);
	string str(ctime(&t));
	str.pop_back();
	return str;
}
