#include "util.hpp"

using namespace KlassDB;

void KlassDB::trim(string& str)
{
	while(!str.empty()) {
		if(str.front() != ' ' && str.front() != '\t') break;
		str.erase(0, 1);
	}

	while(!str.empty()) {
		if(str.back() != ' ' && str.back() != '\t') break;
		str.pop_back();
	}
}
